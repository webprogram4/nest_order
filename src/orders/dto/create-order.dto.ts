import { IsPositive, IsNotEmpty } from 'class-validator';
class CreateOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsPositive()
  @IsNotEmpty()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreateOrderItemDto[];
  customerID: number | FindOperator<number>;
  orderItem: any;
}


